/*
 * CM.JS
 * A microlibrary to help you manage cookies.
 *
 * Tim Severien
 * http://timseverien.nl
 *
 * Copyright (c) 2013 Tim Severien
 * Released under the GPLv2 license.
 *
 */

(function(exports) {

	var date = new Date(0),
		json = exports.JSON,
		stringify = json.stringify,
		encode = encodeURI, decode = decodeURI,
		tryParse = function (obj) {
			try {
				return json.parse(obj);
			} catch (e) {}
			return obj;
		},
		tryStringify = function (obj) {
			if (typeof obj !== 'object' || !stringify) return obj;
			return stringify(obj);
		},
		CM = {
			set: function(name, value, expires, path, domain) {
				var pair = encode(name) + '=' + encode(tryStringify(value));

				if(!!expires) {
					if(expires.constructor === Number) pair += ';max-age=' + expires;
					else if(expires.constructor === String) pair += ';expires=' + expires;
					else if(expires.constructor === Date)  pair += ';expires=' + expires.toUTCString();
				}

				pair += ';path=' + ((!!path) ? path : '/');
				if(!!domain) pair += ';domain=' + domain;

				document.cookie = pair;
			},

			setObject: function(object, expires, path, domain) {
				for(var key in object) this.set(key, object[key], expires, path, domain);
			},

			get: function(name) {
				return this.getObject()[name];
			},

			getObject: function() {
				var pairs = document.cookie.split(/;\s?/i);
				var object = {};

				var pair;

				for(var i in pairs) {
					pair = pairs[i].split('=');

					if(pair.length <= 1) continue;

					object[decode(pair[0])] = tryParse(decode(pair[1]));
				}

				return object;
			},

			unset: function(name) {
				document.cookie = name + '=; expires=' + date.toUTCString();
			},

			clear: function() {
				var obj = this.getObject();

				for(var key in obj) this.unset(key);
			}
		};

	/* AMD support */

	if (typeof define === 'function' && define.amd) {
	      define(function() {
	           return CM;
	       });
	} else {
	     exports.CM = CM;
	}
})(this);
