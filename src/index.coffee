
do (jQuery) ->

  $ = jQuery

  OPTIONS =
    baseUrl: 'https://www.nextgxdx.com'
    themeColor: '#f48604'

  round_color = (value) ->
    return 255 if value > 255
    return 0   if value < 0
    return value

  shade_color = (color, percent) ->
    if color[0] is '#'
      color = color.slice 1

    amount = Math.round 2.55 * percent
    num = parseInt color, 16

    r = round_color (num >> 16) + amount
    g = round_color ((num >> 8) & 0x00FF) + amount
    b = round_color (num & 0x0000FF) + amount

    return '#' + (0x1000000 + r * 0x10000 + g * 0x100 + b).toString(16).slice(1)

  stylesheet = (options) ->
    options.themeColorDark ?= shade_color options.themeColor, -15

    """
      <style>
        html, body {
          background: white;
          color: #666;
          overflow: hidden;
        }

        header, footer {
          display: block;
        }

        * {
          border: 0;
          padding: 0;
          margin: 0;
          box-sizing: border-box;
          -moz-box-sizing: border-box;
          -webkit-box-sizing: border-box;
        }

        a {
          color: inherit;
          text-decoration: none;
        }

        a:hover {
          color: #999;
        }

        h4, h5 {
          overflow: hidden;
          text-overflow: ellipsis;
          white-space: nowrap;
        }

        h4 {
          margin-bottom: 3px;
        }

        h5 {
          font-weight: normal;
        }

        .nxt-cart, .btn {
          font-family: Verdana, Geneva, sans-serif;
        }

        .btn {
          background: #{options.themeColor};
          border: 1px solid #{options.themeColorDark};
          border-radius: 5px;
          box-shadow: 0 1px 2px rgba(0, 0, 0, .1),
                      inset 0px 1px 0px rgba(255, 255, 255, .25);
          color: white;
          cursor: pointer;
          display: inline-block;
          font-size: 14px;
          font-weight: bold;
          padding: 4px 8px;
        }

        .btn.remove {
          padding: 2px 8px 4px;
        }

        .btn.checkout {
          display: block;
          width: 100%;
        }

        .nxt-cart > header {
          background: #{options.themeColor};
          color: white;
          cursor: pointer;
          font-size: 12px;
          font-weight: bold;
          padding: 8px;
          text-align: center;
        }

        .nxt-cart > header > button {
          background: none;
          color: white;
          font-size: 21px;
          font-weight: bold;
          position: absolute;
          right: 14px;
          top: 3px;
        }

        .nxt-cart-item, .nxt-cart > footer {
          border: 1px solid #ccc;
          padding: 10px;
        }

        .nxt-cart-item {
          border-bottom: none;
          display: block;
          list-style: none;
        }

        .nxt-cart-item-buttons {
          display: inline-block;
          float: right;
        }

        .nxt-cart > footer {
          padding-bottom: 20px;
          text-align: right;
          width: 100%;
          position: relative;
        }

        .error {
          background: #a72e25;
          display: none;
          color: white;
          font-size: 10px;
          padding: 5px;
          text-align: center;
        }

        .branding {
          font-size: 8px;
          position: absolute;
          bottom: 5px;
          right: 10px;
        }
      </style>
    """

  setup_frame = (callback) ->
    $('<iframe>',
      src:'javascript:void(document.write("<!DOCTYPE html><html><head></head><body></body></html>"), document.close())'
      frameBorder: 0
      load: callback)
    .css
      bottom: 0
      boxShadow: 'rgba(0, 0, 0, .1) 0 0 3px 2px'
      borderTopLeftRadius: 5
      borderTopRightRadius: 5
      height: 0
      margin: 0
      padding: 0
      position: 'fixed'
      right: 15
      transition: 'height 300ms'
      width: 290
      zIndex: 999
    .appendTo('body')

    return

  checkout_url = (codes, options) ->
    PATH = "#{options.baseUrl}/apps/search/#/order-summary"
    params =
      testCodes: codes
      labCode: options.lab
      referrer: options.referrer or options.lab

    return PATH + '?' + $.param params, true


  Cart = (options) ->
    self = this
    self._products = {}
    self.options = {}

    $.extend self.options, OPTIONS, options

    setup_frame ->
      self.$iframe = $(this)
      self.init()

    return self


  Cart::init = ->
    self = this

    body = this.$iframe.contents().find 'body'

    template = """
                #{stylesheet(this.options)}
                <div class="nxt-cart">
                  <header>
                    <span>Cart (0 items)</span>
                    <button>&ndash;</button>
                  </header>
                  <div class="error"></div>
                  <ul class="nxt-cart-items"></ul>
                  <footer>
                    <button class="btn checkout">Place Order</button>
                    <a class="branding" href="https://www.nextgxdx.com" target="_blank">
                      Powered by NextGxDx
                    </a>
                  </footer>
                </div>
               """

    body.html template

    self.$cart = body.find '.nxt-cart'
    .off 'click'
    .on 'click', '.nxt-cart-items .btn.remove', ->
      code = $(this).closest('.nxt-cart-item').data 'test_code'
      self.remove code
    .on 'click', 'header', ->
      self.$cart.toggleClass 'minimized'
      self.render()
    .on 'click', '.checkout', ->
      window.open checkout_url Object.keys(self._products), self.options

    this.render()

    $.each CM.get('nxt_cart:products') or [], (i, code) ->
      self.add code

    return self


  Cart::set_cookie = ->
    CM.set 'nxt_cart:products', Object.keys this._products


  Cart::remove = (test_code) ->
    if not test_code?
      this._products = {}
    else
      delete this._products[test_code]

    this.set_cookie()
    this.render()

    return this


  Cart::add = (testCode) ->
    self = this
    return self if self._products.hasOwnProperty testCode

    $.ajax "#{self.options.baseUrl}/api/catalog/labtests",
      dataType: 'jsonp'
      data: { labCode: self.options.lab, testCode }
      timeout: 2000
    .done (data) ->
      self._products[data.code] = data
      self.set_cookie()
      self.$cart.removeClass 'minimized'
      self.render()
    .fail ->
      self.error "Product not found: '#{testCode}'."

    return self


  Cart::error = (message, timeout = 5000) ->
    self = this

    $error = self.$cart.find '.error'

    $error.text(message).show()
    self.$iframe.height self.$cart.height()

    setTimeout ->
      $error.text('').hide()
      self.$iframe.height self.$cart.height()
    , timeout

    return self


  Cart::render = ->
    self = this
    codes = Object.keys(this._products).sort()

    $header = self.$cart.find 'header'
    $items = self.$cart.find '.nxt-cart-items'

    items = $.map codes, (code) ->
      p = self._products[code]
      el = $ """
              <li class="nxt-cart-item">
                <div class="nxt-cart-item-buttons">
                  <button class="btn remove">x</button>
                </div>
                <h4>
                  <a href="#{p.url}" target="_blank">#{p.name}</a>
                </h4>
                <h5>#{p.code}</h5>
              </li>
             """

      el.data 'test_code', code
      return el

    $items.html items
    $header.find('> span').text "Cart (#{codes.length} items)"

    if not codes.length
      self.$cart.addClass 'minimized'

    if self.$cart.hasClass 'minimized'
      self.$iframe.height $header.outerHeight()
    else
      self.$iframe.height self.$cart.height()

    return self


  window.$nxt =
    cart: (opts) -> new Cart opts
