
## Usage

```html
<body>
  <script src="//d3806m6vn2o1ma.cloudfront.net/nxt-cart/<version>/nxt.cart.min.js">
  </script>
  <script>
    // Setting options here is equivalent to defining
    // them as data attributes on the script tag (above).
    var cart = $nxt.cart({
      lab: 'LABCODE',       // lab code
      referrer: 'REFERRER', // referral code (if different from lab code)
      themeColor: '#036d94'
    });

    $(document).on('click', '.orderbtn', function (e) {
      var code = $(this).data('test-code');
      cart.add(code);
    });
  </script>
</body>
```
