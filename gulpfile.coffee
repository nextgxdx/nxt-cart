
fs = require 'fs'
gulp = require 'gulp'

clean = require 'gulp-clean'
coffee = require 'gulp-coffee'
concat = require 'gulp-concat'
gulpif = require 'gulp-if'
header = require 'gulp-header'
jade = require 'gulp-jade'
connect = require 'gulp-connect'
s3 = require 'gulp-s3'
uglify = require 'gulp-uglify'
gzip = require 'gulp-gzip'
rename = require 'gulp-rename'

config =
  pkg: require './package.json'

mortar = require('mortar')(config)

banner = """
          /*
           * <%= pkg.name %>
           * <%= pkg.description %>
           *
           * @version v<%= pkg.version %> - <%= pkg.date %>
           * @link <%= pkg.homepage %>
           *
           */
          \n\n
         """

gulp.task 'clean', ->
  gulp.src 'dist', read: false
      .pipe clean()

gulp.task 'build', ->
  pkg = JSON.parse fs.readFileSync './package.json'
  pkg.date = new Date().toISOString().replace /T.*$/, ''

  DEST = "dist/#{pkg.version}/"

  gulp.src [
    'lib/**/*.js'
    'src/**/*.coffee'
  ]
  .pipe gulpif /[.]coffee$/, coffee bare: true
  .pipe concat 'nxt.cart.js'
  .pipe header banner, { pkg }
  .pipe gulp.dest DEST
  .pipe uglify preserveComments: 'all'
  .pipe rename extname: '.min.js'
  .pipe gulp.dest DEST
  .pipe connect.reload()


gulp.task 'watch', [ 'build' ], ->
  gulp.watch [
    'src/**/*'
  ], { read: false }, [ 'build' ]

gulp.task 'deploy', [ 'build' ], ->
  aws =
    key: mortar.config.aws.accessKeyId
    secret: mortar.config.aws.secretAccessKey
    bucket: mortar.config.s3.assets
    region: 'us-east-1'

  options =
    headers:
      'Cache-Control': 'public, max-age=60'
      'Content-Encoding': 'gzip'

  gulp.src "dist/#{config.pkg.version}/*.js"
    .pipe gzip()
    .pipe rename dirname: "#{config.pkg.name}/#{config.pkg.version}"
    .pipe s3(aws, options)

gulp.task 'test:build', ->
  gulp.src 'test/*.jade'
      .pipe jade()
      .pipe gulp.dest 'dist/test/'

  gulp.src 'test/specs/**/*.coffee'
      .pipe coffee()
      .pipe concat 'tests.js'
      .pipe gulp.dest 'dist/test'
      .pipe connect.reload()

gulp.task 'test:server', ->
  connect.server
    root: 'dist'
    port: 9090
    livereload: true

gulp.task 'test:watch', [ 'test:build' ], ->
  gulp.watch [
    'test/**/*'
  ], { read: false }, [ 'test:build' ]


gulp.task 'test', ['test:server', 'test:watch', 'watch']


gulp.task 'default', [
  'watch'
]
